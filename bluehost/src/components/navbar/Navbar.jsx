import React from 'react'
import '../navbar/navbar.css'
const Navbar = () => {
    return (
        <>
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid px-5 pt-2">
                    <a class="navbar-brand pe-3" style={{color:"blue"}} href="#">bluehost</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown pe-3">
                                <a class="nav-link dropdown-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"  >
                                    WordPress
                                </a>
                            </li>
                            <li class="nav-item dropdown pe-3">
                                <a class="nav-link dropdown-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"  >
                                    Web Hosting
                                </a>
                            </li>
                            <li class="nav-item pe-3">
                                <a class="nav-link" href="#">Domains</a>
                            </li>
                            <li class="nav-item dropdown pe-3">
                                <a class="nav-link dropdown-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"  >
                                    Email
                                </a>
                            </li>
                            <li class="nav-item dropdown pe-3">
                                <a class="nav-link dropdown-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"  >
                                    Hire a Pro
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                                <li class="nav-item dropdown pe-3 ">
                                    <a class="nav-link dropdown-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"  >
                                        Support & Resources
                                    </a>
                                </li>
                                <li className="nav-item pe-3">
                                    <a className="nav-link" href="#">
                                        Login
                                    </a>
                                </li>
                                <li class="nav-item dropdown pe-3">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Dropdown
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="#">Action</a></li>
                                        <li><a class="dropdown-item" href="#">Another action</a></li>
                                        <li><hr class="dropdown-divider" /></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </nav>
            <div class="collapse" id="collapseExample">
                <div className="row border">
                    <div className="col-4 px-5 py-5">
                        <div className="col-12">
                            <h5>WordPress Hosting
                                <button type="button" class="btn p-0 px-2 ms-1" style={{backgroundColor:"lightblue", borderRadius:"25px"}}>AI Powered</button>
                            </h5>
                            <p>Automated updates and plugins</p>
                        </div>
                        <div className="col-12">
                            <h5>Cloud Hosting
                                <button type="button" class="btn btn-primary p-0 px-2 ms-1" style={{borderRadius:"25px"}}>New</button>
                            </h5>
                            <p>Lightning-fast with 100% network uptime</p>
                        </div>
                    </div>
                    <div className="col-4 px-5 py-5">
                        <div className="col-12">
                            <h5>WooCommerce
                            </h5>
                            <p>Secure, online store with built-in tools</p>
                        </div>
                        <div className="col-12">
                            <h5>Websites & Online Stores
                            </h5>
                            <p>Powerful site tools and WooCommerce</p>
                        </div>
                    </div>
                    <div className="col-4 px-5 py-5" style={{backgroundColor:"lightblue"}}>
                        <h4 className='fw-bold'>Pro Services</h4>
                        <div className="col-12">
                            <h5>Web Design
                            </h5>
                            <p>Custom WordPress sites built for you</p>
                        </div>
                        <div className="col-12">
                            <h5>Websites & Online Stores
                            </h5>
                            <p>SEO and PPC campaigns done for you</p>
                        </div>
                        <div className="col-12">
                            <h5>Websites & Online Stores
                            </h5>
                            <p>On-demand, WordPress expert support</p>
                        </div>
                    </div>
                </div>
            </div>
            {/* collapse-2 */}
            <div class="collapse" id="collapseExample2">
                <div className="row border">
                    <div className="col-4 px-5 py-5">
                        <div className="col-12">
                            <h5>Web Hosting
                                <button type="button" class="btn p-0 px-2 ms-1" style={{backgroundColor:"lightblue", borderRadius:"25px"}}>AI Powered</button>
                            </h5>
                            <p>AI builder, onboarding, and security</p>
                        </div>
                        <div className="col-12">
                            <h5>Cloud Hosting
                                <button type="button" class="btn btn-primary p-0 px-2 ms-1" style={{borderRadius:"25px"}}>New</button>
                            </h5>
                            <p>Lightning-fast with 100% network uptime</p>
                        </div>
                    </div>
                    <div className="col-4 px-5 py-5">
                        <div className="col-12">
                            <h5>VPS Hosting
                            </h5>
                            <p>Full root access, unlimited bandwidth</p>
                        </div>
                        <div className="col-12">
                            <h5>Dedicated Hosting
                            </h5>
                            <p>Your own single-tenant server</p>
                        </div>
                    </div>
                    <div className="col-4 px-5 py-5" style={{backgroundColor:"lightblue"}}>
                        <h4 className='fw-bold'>Pro Services</h4>
                        <div className="col-12">
                            <h5>Web Design
                            </h5>
                            <p>Custom WordPress sites built for you</p>
                        </div>
                        <div className="col-12">
                            <h5>Websites & Online Stores
                            </h5>
                            <p>SEO and PPC campaigns done for you</p>
                        </div>
                        <div className="col-12">
                            <h5>Websites & Online Stores
                            </h5>
                            <p>On-demand, WordPress expert support</p>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default Navbar
{/* <ul class="dropdown-menu"  data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    <li><a class="dropdown-item" href="#">Action</a></li>
    <li><a class="dropdown-item" href="#">Another action</a></li>
    <li><hr class="dropdown-divider" /></li>
    <li><a class="dropdown-item" href="#">Something else here</a></li>
    <p>

<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
Button with data-bs-target
</button>
</p>
<div class="collapse" id="collapseExample">
<div class="card card-body">
Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
</div>
</div>
</ul> */}