import '../image/bgimage.css'
import React from 'react'

const Bgimage = () => {
    return (
        <>
            <div className="px-5 py-3">
                <div className="row ">
                    <div className="col-lg-7 col-12 ms-4 py-5 my-2" style={{ backgroundColor: "#007FFF", color: "white", borderRadius: "10px" }}>
                        <div className="row">
                            <div className="col-7 ps-5">
                                <h4 className='fw-light pb-3'>WORDPRESS, WEB HOSTING, DOMAINS</h4>
                                <h1 className='display-5 pb-4 fw-bold'>Building your WordPress site just got easier.</h1>
                                <h4 className='fw-light pb-4'>Easily host and launch a stylish WordPress website with our AI WonderSuite tools.</h4>
                                <button type="button" class="btn p-3 px-5" style={{ backgroundColor: "#fdde6c", borderRadius: "0px" }}>Get Started</button>
                                <p className='fw-lighter pt-5'><span>Recommended by</span> <span className='fw-bold'>WordPress.org</span> </p>
                            </div>
                            <div className="col-5">
                                {/* image */}
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-12 ms-4 py-5 px-5 text-center my-2" style={{ backgroundColor: "#034694", color: "white", borderRadius: "10px" }}>
                        <h3 className='py-5'>BLUEHOST CLOUD</h3>
                        <h1 className='disply-3 py-5'>World-class speed for your WordPress sites.</h1>
                        <button type="button" class="btn p-3 px-5 my-4" style={{ backgroundColor: "#fdde6c", borderRadius: "0px" }}>Get Early Access</button>
                    </div>
                </div>

            </div>
            <div class="input-group px-5">
                <input type="text" class="form-control py-3 border-primary border-top border-bottom border-start" style={{ border: "none" }} placeholder="Start brainstorming your domain" aria-label="Recipient's username" aria-describedby="basic-addon2" />

                <select class="pe-2 border-primary border-top border-bottom" style={{ border: "none" }} >
                    <option value="1">.com</option>
                    <option value="2">.net</option>
                    <option value="3">.org</option>
                    <option value="4">.online</option>
                    <option value="5">.site</option>
                    <option value="6">.website</option>
                    <option value="7">.space</option>
                    <option value="8">.tech</option>
                    <option value="9">.store</option>
                    <option value="10">.co</option>
                    <option value="11">.blog</option>
                    <option value="12">.me</option>
                    <option value="13">.biz</option>
                    <option value="14">.info</option>
                    <option value="15">.us</option>
                    <option value="16">.ca</option>
                    <option value="17">.life</option>
                    <option value="18">.cloud</option>

                </select>

                <button class="btn btn-primary px-5 me-4 border-primary" type="button">Search</button>
            </div>


            <div className="px-5 py-5">
                <div className="card p-5" style={{ border: "none", backgroundColor: "#AFDBF5", borderRadius: "15px" }}>
                    <div className="row">
                        <div className="col-md-3 col-12 py-2">
                            <h3 className='fw-bold'>WordPress Hosting
                            </h3>
                            <p className='py-2 '>
                                Get your WordPress site online fast.
                            </p>
                            <h5 style={{ color: "#0076CE", textDecoration: "underline" }}>Learn More &#8594; </h5>
                        </div>
                        <div className="col-md-3 col-12 py-2">
                            <h3 className='fw-bold'>Online Stores
                            </h3>
                            <p className='py-2 '>
                                Sell goods and services effortlessly.
                            </p>
                            <h5 style={{ color: "#0076CE", textDecoration: "underline" }}>Learn More &#8594; </h5>
                        </div>
                        <div className="col-md-3 col-12 py-2">
                            <h3 className='fw-bold'>Cloud Hosting
                            </h3>
                            <p className='py-2 '>
                                Try ultra-fast hosting with 100% uptime.
                            </p>
                            <h5 style={{ color: "#0076CE", textDecoration: "underline" }}>Learn More &#8594; </h5>
                        </div>
                        <div className="col-md-3 col-12 py-2">
                            <h3 className='fw-bold'>Design Services
                            </h3>
                            <p className='py-2 '>
                                Let us handle your website and SEO.
                            </p>
                            <h5 style={{ color: "#0076CE", textDecoration: "underline" }}>Learn More &#8594; </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div className="text-center">
                <p>
                    <span>Our customers say</span>
                    <span className='h6'> Great</span>
                    <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
                    <span> 4.1 out of 5 based on 13,747 reviews </span>
                    <span className='h6'>
                    <span class="fa fa-star"></span> Trustpilot </span>
                </p>
            </div>


        </>
    )
}

export default Bgimage