import Bgimage from './components/image/Bgimage';
import Navbar from './components/navbar/Navbar'
function App() {
  return (
    <div className="App">
      <Navbar/>
      <Bgimage/>
    </div>
  );
}

export default App;
